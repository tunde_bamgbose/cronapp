$(()=>{

    $("#loginForm").validate({
        rules: {
            email: {
                email: true,
                required: true
            },
            password: "required",
        },
        errorPlacement: (err,el)=>{
            if(el.is(":checkbox")){
                el.parents(".checkbox").append(err);
            }
            else{
                el.parents(".form-group").append(err);
            }
        }
    });

    $("#loginForm #btnLogin").on('click',async (e)=>{
        e.preventDefault();

        var _this = $("#loginForm #btnLogin");

        if($("#loginForm").valid()){
            _this.prop('disabled',true);

            var data = {
                email: $("#txtEmail").val(),
                password: $("#txtPassword").val(),
            }

            try{
                let response = await $.ajax({
                    method: "post",
                    url: "/login",
                    xhrFields: {
                        withCredentials: true
                    },
                    data: data
                });

                if(response.status){
                    toastr.success(response.message);
                    location.href = response.redirectTo;
                }
                else{
                    var message = response.error;
                    toastr.error(message);
                }

                _this.prop('disabled',false);
            }
            catch(err){
                toastr.error("Some error occured. Please try again");
                _this.prop('disabled',false);
            }
        }
    })

    $("#signupForm").validate({
        rules: {
            name: "required",
            email: {
                email: true,
                required: true
            },
            password: "required",
            retype: {
                required: true,
                equalTo: "#txtPassword"
            },
            terms: "required"
        },
        errorPlacement: (err,el)=>{
            if(el.is(":checkbox")){
                el.parents(".checkbox").append(err);
            }
            else{
                el.parents(".form-group").append(err);
            }
        }
    });

    $("#signupForm #btnRegister").on('click',async (e)=>{
        e.preventDefault();

        var _this = $("#signupForm #btnRegister");

        if($("#signupForm").valid()){
            _this.prop('disabled',true);

            var data = {
                name: $("#txtName").val(),
                email: $("#txtEmail").val(),
                password: $("#txtPassword").val(),
            }

            try{
                let response = await $.ajax({
                    method: "post",
                    url: "/register",
                    xhrFields: {
                        withCredentials: true
                    },
                    data: data
                });

                if(response.status){
                    toastr.success(response.message);
                    location.href = response.redirectTo;
                }
                else{
                    var message = "";
                    for(var prop in response.error){
                        if(prop == "email"){
                            $("#txtEmail").parents(".form-group").append(`<label id='txtEmail-error' class='error'>${ response.error[prop] }</label>`);
                        }
                        else if(prop == "name"){
                            $("#txtName").parents(".form-group").append(`<label id='txtName-error' class='error'>${ response.error[prop] }</label>`);
                        }
                        else if(prop == "password"){
                            $("#txtPassword").parents(".form-group").append(`<label id='txtPassword-error' class='error'>${ response.error[prop] }</label>`);
                        }
                        message += response.error[prop]+"<br/>";
                    }
                    toastr.error(message);
                }

                _this.prop('disabled',false);
            }
            catch(err){
                toastr.error("Some error occured. Please try again");
                _this.prop('disabled',false);
            }
        }
    })

    $("#btnLogout").on('click',async()=>{
        let result = confirm("You are about to be logged out. Press OK to continue");

        if(result){
            location.href = "/logout";
        }
    })
});