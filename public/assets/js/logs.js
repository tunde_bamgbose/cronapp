var loadLogs = async(id,page=1)=>{
    try{
        if(id){
            let response = await $.ajax({
                method: "get",
                url: `/logs/${ id }?page=${page}`,
                xhrFields: {
                    withCredentials: true
                }
            });
    
            populateServiceTemplate(response.data);

            if(response.meta.total_records > response.meta.per_page){
                populatePaginationTemplate(response.meta);
            }
        }
    }
    catch(err){
        toastr.error(err.error ? err.error : err);
    }
}

var loadGraph = async(id)=>{
    try{
        if(id){
            let response = await $.ajax({
                method: "get",
                url: `/apps/${ id }/graph`,
                xhrFields: {
                    withCredentials: true
                }
            });

            // LINE CHART
            // "use strict";

            var line = new Morris.Line({
                element: 'line-chart',
                resize: true,
                data: response.data,
                xkey: 'date',
                ykeys: ['failures','success'],
                labels: ['Failures','Success'],
                lineColors: ['#DC493C','#3c8dbc'],
                hideHover: 'auto'
            });
        }
    }
    catch(err){
        console.log(err.error ? err.error : err);
    }
}

var populateServiceTemplate = (data)=>{
    let source = $('#logs-template').html();
    let template = Handlebars.compile(source);
    var content = {
        logs : data,
    };
    let html = template(content);
    $("#logsResult").html(html);
}

var populatePaginationTemplate = (meta)=>{
    let source = $('#pagination-template').html();
    let template = Handlebars.compile(source);
    var content = {
        meta : meta,
    };
    let html = template(content);
    $("#paginationSection").html(html);
}

$(()=>{
    loadLogs($("#app_id").val());
    loadGraph($("#app_id").val());

    $(document).on("click",".pageLink",function(){
        var page = $(this).attr("page");
        if(page){
            loadLogs($("#app_id").val(),page);
        }
    });    

        // LINE CHART
        // var line = new Morris.Line({
        //     element: 'line-chart',
        //     resize: true,
        //     data: [
        //     {y: '2011 Q1', item1: 2666},
        //     {y: '2011 Q2', item1: 2778},
        //     {y: '2011 Q3', item1: 4912},
        //     {y: '2011 Q4', item1: 3767},
        //     {y: '2012 Q1', item1: 6810},
        //     {y: '2012 Q2', item1: 5670},
        //     {y: '2012 Q3', item1: 4820},
        //     {y: '2012 Q4', item1: 15073},
        //     {y: '2013 Q1', item1: 10687},
        //     {y: '2013 Q2', item1: 8432}
        //     ],
        //     xkey: 'y',
        //     ykeys: ['item1'],
        //     labels: ['Item 1'],
        //     lineColors: ['#3c8dbc'],
        //     hideHover: 'auto'
        // });
});