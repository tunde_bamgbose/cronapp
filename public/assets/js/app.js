var loadData = async()=>{
    try{
        let response = await $.ajax({
            method: "get",
            url: "/apps/me",
            xhrFields: {
                withCredentials: true
            }
        });

        populateAppTemplate(response.data);
    }
    catch(err){
        toastr.error(err.error ? err.error : err);
    }
}

var populateAppTemplate = (data)=>{
    let source = $('#apps-template').html();
    let template = Handlebars.compile(source);
    var content = {
        apps : data,
    };
    let html = template(content);
    $("#appResults").html(html);
}


$(()=>{

    loadData();

    $.validator.addMethod("regex", (value, element, regexpr)=> {          
        return regexpr.test(value);
      }, "Invaid format"
    ); 

    $("#appForm").validate({
        rules: {
            name: "required",
            url: {
                required: true,
                regex: /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/
            }
        }
    });

    $("#appForm #btnSubmitApp").on('click',async (e)=>{
        e.preventDefault();

        var _this = $("#appForm #btnSubmitApp");

        if($("#appForm").valid()){

            _this.prop('disabled',true);

            var data = {
                name: $("#appName").val(),
                url: $("#baseUrl").val(),
                editID: $("#editID").val(),
            }

            try{
                let response = await $.ajax({
                    method: "post",
                    url: "/apps",
                    xhrFields: {
                        withCredentials: true
                    },
                    data: data
                });

                if(response.status){
                    toastr.success(response.message);
                    populateAppTemplate(response.data);
                    $("#appForm").trigger("reset");

                    if(data.editID){
                        $(document).find("#btnCancelEdit").click();
                    }
                }
                else{
                    var message = response.error;
                    toastr.error(message);
                }

                _this.prop('disabled',false);
            }
            catch(err){
                toastr.error("Some error occured. Please try again");
                _this.prop('disabled',false);
            }
        }
    })

    $("#appForm #btnCancelEdit").on('click',()=>{
        $("#btnSubmitApp").html("Submit");
        $("#btnCancelEdit").addClass("hide");
        $("#appForm").trigger("reset");
        $("#editID").val("");
    });

    $(document).on('click','.btnEditApp',async function(){
        $("#appForm").trigger("reset");
        var _this = $(this);
        var id = _this.data("app-id");
        _this.prop('disabled',true);


        try{
            let response = await $.ajax({
                method: "get",
                url: `/apps/${ id }/details`,
                xhrFields: {
                    withCredentials: true
                }
            });

            if(response.status){
                $("#editID").val(id);
                $("#appName").val(response.data.name);
                $("#baseUrl").val(response.data.url);
                $("#btnSubmitApp").html("Update");
                $("#btnCancelEdit").removeClass("hide");
            }
            else{
                var message = response.error;
                toastr.error(message);
            }

            _this.prop('disabled',false);
        }
        catch(err){
            toastr.error("Some error occured. Please try again");
            _this.prop('disabled',false);
        }
    });

    $(document).on('click','.btnDeleteApp',async function(){
        let result = confirm("Are you sure you want to delete this app? Press OK to continue");

        if(result){
            var _this = $(this);
            var id = _this.data("app-id");

            _this.prop('disabled',true);


            try{
                let response = await $.ajax({
                    method: "post",
                    url: `/apps/${ id }/delete`,
                    xhrFields: {
                        withCredentials: true
                    }
                });
    
                if(response.status){
                    toastr.success(response.message);
                    populateAppTemplate(response.data);
                }
                else{
                    var message = response.error;
                    toastr.error(message);
                }
    
                _this.prop('disabled',false);
            }
            catch(err){
                toastr.error("Some error occured. Please try again");
                _this.prop('disabled',false);
            }
        }

    });

})