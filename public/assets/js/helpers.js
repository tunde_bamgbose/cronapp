module.exports = {
    equals: (value1, value2, options)=>{
        if(value1 === value2)
            return options.fn(this);
    },
    isEqual: (value1, value2) =>{
        if(value1 === value2)
            return true;
        else
            return false;
    }
};