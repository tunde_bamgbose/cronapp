var loadData = async(id)=>{
    try{
        if(id){
            let response = await $.ajax({
                method: "get",
                url: `/apps/${ id }/services`,
                xhrFields: {
                    withCredentials: true
                }
            });
    
            populateServiceTemplate(response.data);
        }
    }
    catch(err){
        toastr.error(err.error ? err.error : err);
    }
}

var populateServiceTemplate = (data)=>{
    let source = $('#services-template').html();
    let template = Handlebars.compile(source);
    var content = {
        services : data,
    };
    let html = template(content);
    $("#servicesResult").html(html);
}

$(()=>{
    loadData($("#appID").val());

    $("#ddlSchedule").on('change',()=>{
        var value = $("#ddlSchedule").val();
        if(value === "custom")
            $("#txtSchedule").parent().removeClass("hide");
        else
            $("#txtSchedule").parent().addClass("hide");
    });

    $("#serviceForm").validate({
        rules: {
            endpoint: "required",
            custom: "required",
            schedule: {
                required: function(el){
                    return $("#ddlSchedule").val() == "custom"
                }
            }
        }
    })

    $("#serviceForm #btnSubmitService").on('click',async (e)=>{
        e.preventDefault();

        var _this = $("#serviceForm #btnSubmitService");

        if($("#serviceForm").valid()){

            _this.prop('disabled',true);

            var data = {
                endpoint: $("#txtEndpoint").val(),
                custom: $("#ddlSchedule").val(),
                schedule: $("#ddlSchedule").val() == "custom" ? $("#txtSchedule").val() : $("#ddlSchedule").val(),
                app_id: $("#app-id").val(),
                service_id: $("#serviceID").val()
            }

            try{
                let response = await $.ajax({
                    method: "post",
                    url: "/services",
                    xhrFields: {
                        withCredentials: true
                    },
                    data: data
                });

                if(response.status){
                    toastr.success(response.message);

                    if(!data.service_id){
                        $("#serviceForm").trigger("reset");
                    }
                    
                }
                else{
                    var message = response.error;
                    toastr.error(message);
                }

                _this.prop('disabled',false);
            }
            catch(err){
                toastr.error("Some error occured. Please try again");
                _this.prop('disabled',false);
            }
        }
    })

    $(document).on("click",".btnDeleteService",async function(){
        try{
            var _this = $(this);
            var id = _this.data("service-id");

            let result = confirm("Are you sure you want to delete this service? Action cannot be reversed. Press OK to continue");

            if(result){

                _this.prop("disabled", true);

                let response = await $.ajax({
                    method: "post",
                    url: `/services/${ id }/delete`,
                    xhrFields: {
                        withCredentials: true
                    }
                });

                if(response.status){
                    toastr.success(response.message);
                    populateServiceTemplate(response.data);
                    _this.prop("disabled", false);
                }
                else{
                    toastr.error(response.error);
                    _this.prop("disabled", false);
                }
            }
        }
        catch(err){
            toastr.error(err.error ? err.error : err);
        }
    })

    $(document).on('click','.btnEnableService',async function(){
        var _this = $(this);
        var id = _this.data('service-id');
        var status = _this.attr('status');

        status = status == "true" ? false : true;
        var statusText = status ? "enable": "stop";
        
        try{
            let result = confirm(`Are you sure you want to ${statusText} this service? Press OK to continue`);

            if(result){

                _this.prop("disabled", true);

                var data = {
                    status: status
                }

                let response = await $.ajax({
                    method: "post",
                    url: `/services/${ id }/enable`,
                    xhrFields: {
                        withCredentials: true
                    },
                    data: data
                });

                if(response.status){
                    toastr.success(response.message);
                    var label = _this.parents("tr").find(".status span.label");
                    var button = _this.parents("tr").find(".btnEnableService");

                    if(data.status){         
                        //label        
                        label.removeClass("label-warning").addClass("label-info");
                        label.html("Running");
                        //button
                        button.removeClass("btn-success").addClass("btn-light");
                        button.attr("status",status);
                        button.html("Stop");
                    }
                    else{
                        //label
                        label.removeClass("label-info").addClass("label-warning");
                        label.html("Stopped");
                        //button
                        button.removeClass("btn-light").addClass("btn-success");
                        button.attr("status",status);
                        button.html("Start");
                    }
                    _this.prop("disabled", false);
                }
                else{
                    toastr.error(response.error);
                    _this.prop("disabled", false);
                }
            }
        }
        catch(err){
            toastr.error(err.error ? err.error : err);
        }
    })
});