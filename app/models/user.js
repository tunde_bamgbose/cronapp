var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

var userSchema = Schema({
    name: { type : String , required: [true,'Name field is required'] },
    email: { type: String, required: [true,'Email field is required'], unique: true, match: [emailRegex,'Invalid email address']},
    password: { type: String, required: [true,'Password field is required'] },
    profile_picture: { type: String },
    created_at: { type: Date, default: Date.now() }
})

var User = module.exports = mongoose.model('User', userSchema);