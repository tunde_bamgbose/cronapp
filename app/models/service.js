var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var serviceSchema = Schema({
    endpoint: { type: String, required: true },
    schedule : { type: String, required: true},
    isCustom: { type: Boolean, default: false}, // is schedule defined by user?
    description: { type: String, required: false },
    app_id: { type: Schema.Types.ObjectId, ref: 'App', required: true },
    user_id: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    created_at: { type: Date, default: Date.now() },
    status: { type: Boolean }, // true - enabled, false - disabled
});

var Service = module.exports = mongoose.model('Service',serviceSchema);