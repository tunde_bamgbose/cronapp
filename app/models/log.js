var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var logSchema = Schema({
    created_at: { type: Date, default: Date.now()},
    service_id: { type: Schema.Types.ObjectId, required: true, ref: 'Service' },
    app_id: { type: Schema.Types.ObjectId, required: true, ref: 'App' },
    action: { type: String, required: true },
}); 

var Log = module.exports = mongoose.model('Log',logSchema);