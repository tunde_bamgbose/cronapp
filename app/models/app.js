var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var appSchema = Schema({
    name :{ type: String, required: true },
    url: { type: String, required: true },
    user_id: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    created_at: { type: Date, default: Date.now() },
    services: { type: Number, default: 0 }
});

var App = module.exports = mongoose.model('App',appSchema);