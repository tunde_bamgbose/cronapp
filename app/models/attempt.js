var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var attemptSchema = Schema({
    created_at: { type: Date, default: Date.now()},
    service_id: { type: Schema.Types.ObjectId, required: true, ref: 'Service' },
    app_id: { type: Schema.Types.ObjectId, required: true, ref: 'App' },
    url: { type: String, required: true },
    status: { type: Boolean }, // true - succesful. false - failed
    statusCode: { type: String },
    response: { type: String }
}); 

var Attempt = module.exports = mongoose.model('Attempt',attemptSchema);