var authController = require('../controllers/AuthController');
var dashboardController = require('../controllers/DashboardController');
var appsController = require('../controllers/AppsController');
var serviceController = require('../controllers/ServiceController');

module.exports = (app)=>{

    app.get('/', (req,res)=>{
        res.render('index',{ title : "Home", layout: 'home' });
    });

    /**
     * Auth Guard. Protect pages from unauthorized access
     */
    var authGuard = (req,res,next)=>{
        // check if user has a session active
        if(req.session.user){
            next(); // if session exists, proceed to next action
        }
        else{
            // redirect user to login page
             res.redirect('/login');
        }
    }

    /**
     * Error Pages
     */
    app.get('/404', (req, res) => {
        res.render('errors/404',{ title: "Page Not Found", layout: "auth"} );
    });
    
    /**
     * Auth Routes
     */
    app.get('/login', authController.login);
    app.post('/login', authController.login);

    app.get('/register', authController.register);
    app.post('/register', authController.register);
    
    app.get('/logout', authController.logout);


    app.get('/dashboard', authGuard ,dashboardController.dashboard);

    /**
     * My Apps Route
     */
    app.get('/apps', authGuard , appsController.index); // view - get apps view
    app.get('/apps/me', authGuard , appsController.list); // ajax - get apps of current user
    app.post('/apps', authGuard, appsController.create);  // ajax - create and update app
    app.post('/apps/:id/delete', authGuard ,appsController.delete); // ajax - delete app
    app.get('/apps/:id', authGuard ,appsController.getApp); // 
    app.get('/apps/:id/details', authGuard ,appsController.details);
    
    app.get('/apps/:app_id/logs', authGuard ,appsController.getServiceLogs);
    app.get('/apps/:app_id/graph', authGuard ,appsController.getAppLogsGraph);
    app.get('/logs/:app_id', authGuard ,appsController.getAppLogs);

    /**
     * Service routes
     */
    app.get('/apps/:id/services', authGuard ,serviceController.list);
    app.get('/apps/:id/service/new', authGuard , serviceController.index);
    app.post('/services', authGuard, serviceController.create);
    app.post('/services/:id/delete', authGuard, serviceController.delete);
    app.get('/apps/:id/service/:service_id/edit', authGuard ,serviceController.index);
    app.post('/services/:id/enable', authGuard, serviceController.enable);

    // app.get('*', (req, res) => {
    //      res.redirect('/404');
    // });

};