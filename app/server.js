// require express
const express = require('express'); 

// load config
const config = require('app-config').config;

// initialize express
const app = express();


// user bodyparser
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// require express session
var session = require('express-session');
var cookieParser = require('cookie-parser');

// require mongoose 
var mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);

// configure session to use mongodb
var mongoStore = require('connect-mongo')(session);

// connect to database
mongoose.connect(`mongodb://${ config.db.user }:${ config.db.password }@${ config.db.host }/${ config.db.name }`, { useCreateIndex: true, useNewUrlParser: true });

var db = mongoose.connection;

// set up session
app.use(cookieParser());
app.use(session({
    secret: "config.session.secret",
    resave: true,
    saveUninitialized: false,
    store: new mongoStore({
        mongooseConnection: db
    })
}));

// load app routes
require('./routes/routes')(app);

db.on("error",(err)=>{
    console.log("DB Error: ",err);
});



//define static files location
var path = require('path');
app.use(express.static(path.join(__dirname,"../public")));

// set view engine to use express handlebars
var exhbs = require('express-handlebars');
app.engine('.hbs',exhbs({
    defaultLayout: "default",
    extname: ".hbs",
    layoutsDir: path.join(__dirname,"views/layouts"),
    helpers: require('../public/assets/js/helpers')
}));
app.set('view engine','.hbs');
app.set('views',path.join(__dirname,"views"));


var Service = require('./controllers/ServiceController');
var Cron = require('./controllers/CronController');
// start server
var server = app.listen(config.app.port || 3000, async() => {
    console.log(`Listening on ${ server.address().address }:${ server.address().port }`);

    // get all services that are currently active and start them
    try{
        let services = await Service.getActive();
        if(services.length > 0){
            // create cron for each service
            services.forEach((service)=>{
                let task = Cron.scheduleTask(service,service._id);
                // start task
                task.start();
            });
        }
    }
    catch(err){};
});