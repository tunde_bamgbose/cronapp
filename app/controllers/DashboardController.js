exports.dashboard = (req,res)=>{
    var session = req.session;
    var user = session.user
    res.render('dashboard/index',{ title: "Dashboard", user: user });
};