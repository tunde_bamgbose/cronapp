var Service = require("../models/service");
var App = require("../models/app");
var tasks = require('../data/tasks');
var Log = require('../models/attempt');
var mongoose = require('mongoose');

/**
 * This function render the view to create or update a service
 * @param {string} app_slug
 * @returns view
 */
exports.index =  async(req,res)=>{
    var session = req.session;
    var user = session.user

    try{
        let app = await App.findById(req.params.id);

        if(!app){
            res.redirect('/404');
            return;
        }

        var appName = app.name;
        var subTitle = req.params.service_id ? "Edit Service" : "Create New Service";
        var service = null;

        var breadcrumb = [
            { url: `/apps/${req.params.id}`, title:  appName },
            { active: true, title:  subTitle }
        ];

        var scheduleOptions = [
            { "text": "Every Minute", value: "* * * * *","selected": false },
            { "text": "Custom", value: "custom", "selected": false },
        ];

        if(req.params.service_id){
            service = await Service.findById(req.params.service_id);

            for(var item of scheduleOptions){
                if(service.isCustom && item.value == "custom"){
                    item.selected = true;
                }
                else if(service.schedule == item.value){
                    item.selected = true;
                }
            }
        }
        res.render('services/create',{ user: user, page: "AppServices", service: service, title: appName, app: app, subTitle: subTitle, scheduleOptions: scheduleOptions, breadcrumb : breadcrumb  });
    }
    catch(err){
        res.redirect('/500');
    }
};

/**
 * This function list all the services associated with an app id
 * @param { string } id app id
 * @return json
 */
exports.list = async (req,res)=>{
    var session = req.session;
    var user = session.user;

    try{
        let app_id = req.params.id;
        let query = { user_id: user.id, app_id : app_id };
        let services = await Service.find(query).sort({ created_at: -1});
        res.send({status: true, data: services});
    }
    catch(error){
        res.send({staus: false, error: error});
    }
}


var cron = require('../controllers/CronController');

/**
 * This function creates and updates a service
 * @param { string } id service id (optional)
 * @param { string } app_id app id
 * @param { string } endpoint endpoint to be called on base url
 * @param { string } schedule schedule to run service
 * @param { string } description simple description about service
 * @param { boolean } custom is schedule a custom schedule? (OPTIONAL)
 * @return json   
 */
exports.create = async(req,res)=>{
    try{
        var session = req.session;
        var user = session.user;

        var data = {
            endpoint: req.body.endpoint,
            description: req.body.description,
            schedule: req.body.schedule,
            isCustom: req.body.custom == "custom" ? true : false,
            user_id: user.id,
            app_id: req.body.app_id
        };

        
        if(req.body.service_id){
            // check if cron schedule is valid
            if(cron.validate(data.schedule)){
                var service = await Service.findById(req.body.service_id);
                service.endpoint = data.endpoint;
                service.description = data.description;
                service.schedule = data.schedule;
                service.isCustom = data.isCustom;
                await service.save();
                message = "Service Successfully Updated";
            }
            else{
                res.send({ status: false, error: "Invalid cron schedule"});
                return;
            }
        }
        else{
            // check if cron schedule is valid
            if(cron.validate(data.schedule)){
                var service = new Service(data);
                await service.save();
                var app = await App.findById(data.app_id);
                app.services += 1;
                await app.save();
                message = "Service Successfully Created";
            }
            else{
                res.send({ status: false, error: "Invalid cron schedule"});
                return;
            }
        }
        
        res.send({status: true, message: message});
    }
    catch(err){
         // handle validation error
         var errors = {}

         if(err.name == "ValidationError"){
 
            if(err.errors.endpoint){
                errors["endpoint"] =  err.errors.endpoint.message;
            }
 
            if(err.errors.description){
                errors["description"] =  err.errors.description.message;
            }

            if(err.errors.schedule){
                errors["schedule"] =  err.errors.schedule.message;
            }
 
             res.send({ status: false, error: errors});
         }
         else{
             res.send({ status: false, error: err});
         }
    }
}

/**
 * This function deletes a service, decrements the total services under apps, stop cron if active and cleans up attempts log
 * @param { string } id service id
 * @return json
 */
exports.delete = async(req,res)=>{
    try{
        var session = req.session;
        var user = session.user;
        var id = req.params.id;
        let service = await Service.findByIdAndRemove(id);

        if(service){
            let app = await App.findById(service.app_id);
            app.services -= 1;
            await app.save();
            
            // stop cron for if service was active
            if(service.status){
                // stop cron
                // get reference to task object
                var task = tasks[id];
                // destroy and stop task
                task.destroy();
                // remove task from tasks variable
                delete tasks[id];
            }
            // sanitize logs.
            let logs = await Log.deleteMany({ service_id : mongoose.Types.ObjectId(id)});

            let query = { user_id: user.id, app_id : app._id };
            let services = await Service.find(query).sort({ created_at: -1});
            res.send({ status: true, message: "Service deleted", data: services});
        }
        else{
            res.send({ status: false, error: "Service Not Found"});
        }
    }
    catch(err){
        res.send({ status: false, error: err});
    }
}


/**
 * This method enable and disables a service
 * @param { string } id service id
 * @param { boolean } status boolean to enable or disable service
 */
exports.enable = async (req,res)=>{
    try{
        var id = req.params.id;
        var status = req.body.status;
        let service = await Service.findById(id).populate('app_id');

        if(service){
            if(status == "true"){
                // schedule new task
                let task = await cron.scheduleTask(service,id);
                // start task
                task.start();
                // update status on db to enabled
                service.status = true;
                await service.save();
                var message = "Service Started";
            }
            else{
                // get reference to task object
                var task = tasks[id];
                // destroy and stop task
                task.destroy();
                // remove task from tasks variable
                delete tasks[id];
                // update status on db to disabled
                service.status = false;
                await service.save();
                var message = "Service Stopped";
            }
            
            res.send({ status: true, message: message, data: service});
        }
        else{
            res.send({ status: false, error: "Service Not Found"});
        }
    }
    catch(err){
        res.send({ status: false, error: err});
    }
}

/**
 * This method fetch list of active services
 * @return { Array } Array of services
 * @throws throws an exception if error occurs
 */
exports.getActive = async()=>{
    try{
        var services = await Service.find({ status: true }).populate('app_id');
        return services;
    }
    catch(err){
        console.log("Error fetch active services:", err);
    }
}