var Attempt = require('../models/attempt');
var Log = require('../models/log');
/**
 * This function inserts cron attempts into database
 * @param { object } data Log data
 */
exports.attempt = async (data)=>{
    try{
        var attempt = new Attempt(data);
        let res = await attempt.save();   
    }
    catch(err){
        console.log("Unable to log response: ",err);
    }
}

/**
 * This function inserts logs into database
 * @param { object } data Log data
 */
exports.log = async (data)=>{
    try{
        var log = new Log(data);
        let res = await log.save();   
    }
    catch(err){
        console.log("Unable to log response: ",err);
    }
}