var cron = require('node-cron');
var Request = require('request');
var logs = require('./LogController');
var mongoose = require('mongoose');
/**
 * Verify if a cron schedule is valid
 * @param { string } schedule
 * @return { boolean } true if valid
 */
exports.validate = (schedule)=>{
    return cron.validate(schedule);
}


var tasks = require('../data/tasks');
/**
 * This method is used to schedule a task
 * @param { object } service service object
 * @param { string } key key to save reference to scheduled task
 * @return { circular } scheduled task
 */
exports.scheduleTask = (service,key)=>{
    tasks[key] = new cron.schedule(service.schedule,()=>{
        // make http call here
        var endpoint = service.app_id.url+"/"+service.endpoint;

        console.log("Running cron on ",endpoint);

        Request.get(`${ endpoint }`,async(err,res,body)=>{
            if(err){
                var log = {
                    service_id: mongoose.Types.ObjectId(service._id),
                    app_id: mongoose.Types.ObjectId(service.app_id._id),
                    url: endpoint,
                    status: false,
                    statusCode: res && res.statusCode ? res.statusCode : "N/A",
                    response: JSON.stringify(err)
                };
            }
            else{
                var log = {
                    service_id: mongoose.Types.ObjectId(service._id),
                    app_id: mongoose.Types.ObjectId(service.app_id._id),
                    url: endpoint,
                    status: res && res.statusCode == "200" ? true : false,
                    statusCode: res && res.statusCode ? res.statusCode : "N/A",
                    response: JSON.stringify(body)
                };
            }
            await logs.attempt(log);
            
        });
    },{ scheduled: false });

    return tasks[key];
}