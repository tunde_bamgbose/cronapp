var App = require('../models/app');
var Service = require('../models/service');
var Log = require('../models/attempt');
var mongoose = require('mongoose');
var tasks = require('../data/tasks');
const config = require('app-config').config;

/**
 * This renders the list view
 * @returns view
 */
exports.index = async(req,res)=>{
    var session = req.session;
    var user = session.user;

    var breadcrumb = [
        { active: true, title: "My Apps"}
    ];

    res.render('apps/index',{ title: "My Apps", user: user, page: "MyApps", breadcrumb: breadcrumb });
};

/**
 * This function gets the list of apps created by current user
 * @return {json} status is true is succesful and false if an error occurs
 */
exports.list = async(req,res)=>{
    var session = req.session;
    var user = session.user;

    try{
        let apps = await getUserApps(user.id);
        res.send({ status: true, data: apps});
    }
    catch(err){
        res.send({ status: false, error: err});
    }
};

/**
 * This function get a single app and render the view
 * @param {string} app_slug
 * @return view
 */
exports.getApp = async (req,res)=>{
    try{
        var session = req.session;
        var user = session.user
        let app = await App.findById(req.params.id,{ name: 1, url : 1, services: 1  });
        if(app){
            var breadcrumb = [
                { active: true, title: app.name }
            ];
            res.render('apps/single',{ title: "Services", user: user, app: app, page: "AppServices", breadcrumb: breadcrumb  });
        }
        else{
            res.redirect('/404');
        }
    }
    catch(err){
        res.redirect('/404');
    }
};


/**
 * This function gets and renders the view for service logs
 * @param {string} app_slug
 * @return view
 */
exports.getServiceLogs = async(req,res)=>{
    var session = req.session;
    var user = session.user;
    try{
        var app = await App.findById(req.params.app_id);
        if(!app){
            res.redirect('/404');
            return;
        };
        var appName = app.name;
        var breadcrumb = [
            { url: `/apps/${req.params.app_id}`, title:  appName },
            { active: true, title:  "Logs" }
        ];
        res.render('apps/logs',{ title: appName, user: user, app: app, page : "Logs", breadcrumb: breadcrumb  });
    }
    catch(err){
        res.redirect('/500');
    }
};


/**
 * This function gets log of app services
 * @param {string} app_id
 * @return { json }
 */
exports.getAppLogs = async(req,res)=>{
    var session = req.session;
    var user = session.user;
    try{
        var per_page = config.app.per_page;
        var page = req.query.page ? req.query.page : 1;
        var skip = page == 1 ? 0 : (page * per_page) - per_page;

        // paginate data
        var [result, count] = await Promise.all([
            Log.find({ app_id : mongoose.Types.ObjectId(req.params.app_id) }).skip(skip).limit(per_page).populate("service_id"),
            Log.countDocuments({ app_id : mongoose.Types.ObjectId(req.params.app_id) })
        ]);

        // add meta key for pagination
        meta = {};
        meta["current_page"] = page;
        meta["per_page"] = per_page;
        meta["prev_page"] = page > 1 ? parseInt(page) - 1 : null;
        meta["total_pages"] = Math.ceil(count/per_page);
        meta["next_page"] = parseInt(page) + 1 > meta["total_pages"] ? null : parseInt(page) + 1; 
        meta["total_records"] = count;
        
        res.send({ status: true, data: result, meta: meta });
    }
    catch(err){
        res.send({status: false, error: err});
    }
};

exports.getAppLogsGraph = async(req,res)=>{
    var session = req.session;
    var user = session.user;
    try{
        var defaultDateLimit = 14;
        var date = new Date();
        var today = date.toDateString();
        var startDate = new Date(date.setDate(date.getDate() - defaultDateLimit)).toDateString() // 14 days back

        data = [];

        var result = await Log.find({ 
            app_id : mongoose.Types.ObjectId(req.params.app_id),
            created_at:  {
                $gte: startDate,
                $lte: today
            } 
        },"created_at status");

        var temp = {};

        for(var $i = 1; $i <= defaultDateLimit; $i++){
            date = new Date();
            var x_axis = new Date(date.setDate(date.getDate() - $i)).toDateString();
            temp[x_axis] = {
                failures: 0,
                success: 0
            };
        }

        if(result.length > 0){
            result.forEach((res)=>{
                var current_date = new Date(res.created_at).toDateString();
                if(res.status){
                    temp[current_date]["success"] += 1 ;
                }
                else{
                    temp[current_date]["failures"] += 1 ;
                }
            });
        }

        var keys = Object.keys(temp);

        for(var key of keys){
            var obj = {};
            obj["date"] = new Date(key);
            obj["failures"] = temp[key]["failures"];
            obj["success"] = temp[key]["success"];
            data.push(obj);
        }
        
        res.send({ status: true, data: data });
    }
    catch(err){
        res.send({status: false, error: err});
    }
}

/**
 * This function is used to create an app
 * @param { string } name app name
 * @param { string } url base url
 * @return { json } status is true if successful and false if failed
 */
exports.create = async (req,res)=>{
    try{
        var session = req.session;
        var user = session.user;
        req.body.user_id = user.id;

        if(!req.body.editID){
            let app = new App(req.body);

            // save new app for user
            let newApp = await app.save();
            message = "App Successfully Created";
        }
        else{
            let app = await App.findById(req.body.editID);
            app.name = req.body.name;
            app.url = req.body.url;
            await app.save();
            message = "App Successfully Updated";
        }

        let apps = await getUserApps(user.id);

        res.send({ status: true, message: message, data: apps});
    }
    catch(err){
        // handle validation error
        var errors = {}

        if(err.name == "ValidationError"){

            if(err.errors.name){
                errors["name"] =  err.errors.name.message;
            }

            if(err.errors.url){
                errors["url"] =  err.errors.url.message;
            }

            res.send({ status: false, error: errors});
        }
        else{
            res.send({ status: false, error: err});
        }
    }
}



/**
 * This function deletes an app
 * @param {String} id app id
 * @return { json } status is true if successful and false if failed. data contains list of apps
 */
exports.delete = async(req,res) => {
    var session = req.session;
    var user = session.user;
    try{
        let app = await App.deleteOne({ _id: req.params.id });
        
        //get services associated with app and delete them
        var id = mongoose.Types.ObjectId(req.params.id);
        let services = await Service.find({ app_id : id });

        // if services existed, delete service, stop cron for each active service
        if(services.length > 0){
            services.forEach(async(service) => {
                await Service.deleteOne({ _id: service._id });

                if(service.status){
                    // stop cron
                    // get reference to task object
                    var task = tasks[service._id];
                    // destroy and stop task
                    task.destroy();
                    // remove task from tasks variable
                    delete tasks[service._id];
                }
            });
        }
        // sanitize logs.
        let logs = await Log.deleteMany({ app_id : mongoose.Types.ObjectId(req.params.id)});

        let apps = await getUserApps(user.id);
        res.send({status: true, message: "App Deleted", data: apps});
    }
    catch(err){
        res.send({ status: false, error: err});
    }
}

/**
 * This function gets details an app
 * @param {String} id app id
 * @return { json } status is true if successful and false if failed. data contains list of apps
 */
exports.details = async(req,res) => {
    try{
        let app = await App.findById(req.params.id,{ name: 1, url : 1, services: 1  });
        res.send({status: true, data: app});
    }
    catch(err){
        res.send({ status: false, error: err});
    }
}

var getUserApps = async (id)=>{
    return await App.find({ user_id: id }, { name: 1, url : 1, services: 1  }).sort({ created_at: -1});
}