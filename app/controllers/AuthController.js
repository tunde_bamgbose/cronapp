var User = require('../models/user');
var bcrypt = require('bcrypt');
const SALT_ROUNDS = 10;

/**
 * @method /GET
 * @return view
 *
 * @method /Post
 * @param { String } email user's email
 * @param { String } password user's password
 * 
 * @return json status is false is request failed, else status is true
 */
exports.login = async(req,res)=>{
    

    // if request is a get request, then return view. else process Post request
    if(req.method == 'GET'){
        res.render('auth/login',{ title: "Login", layout: "auth" });
        return;
    }

    try{
        // check if email and password is present
        if(!req.body.email || !req.body.password){
            res.send({ status: false, error: "Email and Password field is required"});
            return;
        }

        var email = req.body.email;
        var password = req.body.password;

        // get user with email address
        let user = await User.findOne({ "email": email });

        if(user){
            // user found. 
            // hash password and compare to stored value
            let result = await bcrypt.compare(password,user.password);

            if(result){
                // set session 
                await startSession(req,user);

                res.send({ status: true, message: "Authentication successful", redirectTo: "/apps"});
            }
            else{
                res.send({ status: false, error: "Invalid email/password combination"});
            }
        }
        else{
            res.send({ status: false, error: "Invalid credentials provided"});
        }

    }
    catch(err){
        res.send({ status: false, error: err});
    }

};

/**
 * @method /GET
 * @return view
 *
 * @method /Post
 * @param { String } name user's name
 * @param { String } email user's email
 * @param { String } password user's password
 * 
 * @return json status is false is request failed, else status is true
 */
exports.register =  async (req,res)=>{

    // if request is a get request, then return view. else process Post request
    if(req.method == 'GET'){
        res.render('auth/register',{ title: "Register", layout: "auth" });
        return;
    }

    try{
        var user = new User(req.body);

        // hash user password
        var hashedPassword = user.password ? await bcrypt.hash(user.password,SALT_ROUNDS) : "";

        // replace typed user password
        user.password = hashedPassword;

        // create new user account
        let newUser = await user.save();

        // set session 
        await startSession(req,newUser);

        res.send({ status: true, message: "User account created", redirectTo: "/apps"});
        
    }
    catch(err){
        // handle validation error
        var errors = {}

        if(err.name == "ValidationError"){

            if(err.errors.name){
                errors["name"] =  err.errors.name.message;
            }

            if(err.errors.email){
                errors["email"] =  err.errors.email.message;
            }

            if(err.errors.password){
                errors["password"] =  err.errors.password.message;
            }

            res.send({ status: false, error: errors});
        }
        else if(err.name == "MongoError" && err.code == "11000"){
            // error occurred inserting into unique email address field
            errors.email = "Email already registered";
            res.send({ status: false, error: errors});
        }
        else{
            res.send({ status: false, error: err});
        }
        
    }

};

/**
 * This function logs a user out and destroys session
 * 
 * @method /GET
 * @redirect view redirects to login on success
 */
exports.logout = async (req,res)=>{
    try{
        // destroy session  
        await req.session.destroy();
        res.redirect('/login');
    }
    catch(err){
        res.redirect('/500');
    }
}; 


/**
 * Set session variable
 * 
 * @param req request header object
 * @param { object } user user information
 * 
 * @return Promise promise that operation finished
 */
var startSession = (req,user)=>{
    return new Promise((resolve,reject)=>{
        req.session.user = {
            id: user._id,
            name: user.name,
            email: user.email
        };

        resolve(true);
    })
}